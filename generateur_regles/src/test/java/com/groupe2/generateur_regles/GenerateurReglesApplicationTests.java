package com.groupe2.generateur_regles;

import com.groupe2.generateur_regles.repository.*;
import com.groupe2.generateur_regles.models.*;
import com.groupe2.generateur_regles.services.GenerateurService;
import com.groupe2.generateur_regles.services.JpaUserService;
import org.jsoup.Jsoup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GenerateurReglesApplicationTests {
	@Autowired
	private HistoriqueRepository historiqueRepository;
	@Autowired
	GenerateurService generateurService;
	@Autowired
	private CategorieRepository categorieRepository;
	@Autowired
	private SousCategorieRepository sousCategorieRepository;
	@Autowired
	private RegleRepository regleRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private  JpaUserService jpaUserService;
	@Test
	@Transactional
	void megatest (){
		this.modifierRegle();
		this.deleteRegle();
	}

	@Test
	@Transactional
	void deleteRegle(){
		long nbrRegle=regleRepository.count();
		Regle regle=regleRepository.getOne(14L);
		regleRepository.delete(regle);
		assertThat(regleRepository.count()).isEqualTo(nbrRegle-1);
		//assertThat(regleRepository.count()).isLessThan(nbrRegle);
	}

	@Test
	@Transactional
	void modifierRegle(){
		Regle regle=regleRepository.getOne(14L);
		String excorpus=regle.getCorpus();
		assertThat(excorpus).isNotEqualTo("test modification regle");
		regle.setCorpus("test modification regle");
		assertThat(regle.getCorpus()).isEqualTo("test modification regle");
		//assertThat(regle.getCorpus()).isNotEqualTo(excorpus);
		regleRepository.save(regle);
		Regle regle2=regleRepository.getOne(14L);
		assertThat(regle2.getCorpus()).isEqualTo("test modification regle");
		//assertThat(regle2.getCorpus()).isNotEqualTo(excorpus);
	}
	@Test
	@Transactional
	void injectionHTMLSouSCategorie(){
		SousCategorie scat=new SousCategorie();
		//Création d'une nouvelle sous-catégorie
		scat.setObjet("une chaine <script>alert( 'Hello, world!' );</script>");
		//set Objet utilise Jsoup pout parser le code HTML
		assertThat(scat.getObjet()).isEqualTo("une chaine");
		//vérification que il n'y a plus de balise HTML
	}

	@Test
	@Transactional
	void testServiceLastvs(){
		List <Categorie> categories = generateurService.lastvs();
		//On utilise notre service pour construire la liste de categories qu'ils nous faut
		for (Categorie cat:categories)
		{
			Categorie derniereVersionCat= categorieRepository.findAllByNewversionIsNullAndCode(cat.getCode()).get(0);
			assertThat(cat.getId()).isEqualTo(derniereVersionCat.getId());
			//Verification que l'id corespond bien a la dernière version.
			assertThat(cat.getTitre()).isEqualTo(derniereVersionCat.getTitre());
			//Verification que les cat de la liste on bien la dernière version du titre
			Categorie premiereVersionCat= categorieRepository.findAllByExversionIsNullAndCode(cat.getCode()).get(0);
			List <SousCategorie> listScatPremiereVersion=premiereVersionCat.getSousCategories();
			assertThat(listScatPremiereVersion).containsAll(cat.getSousCategories());
			//Verification que les sous-catégories de la cat sont
			// toutes bien dans la les sous-cat de la premiere version de la cat
			for (SousCategorie scat:cat.getSousCategories())
			{
				SousCategorie derniereVersionSCat= sousCategorieRepository.findAllByNewversionIsNullAndNumordreAndCategorie(scat.getNumordre(),premiereVersionCat).get(0);
				assertThat(derniereVersionSCat.getId()).isEqualTo(scat.getId());
				assertThat(derniereVersionSCat.getTitre()).isEqualTo(scat.getTitre());
				assertThat(derniereVersionSCat.getObjet()).isEqualTo(scat.getObjet());
				//Verificaton du Titre et de l'objet
				SousCategorie premiereVersionScat= sousCategorieRepository.findAllByExversionIsNullAndNumordreAndCategorie(scat.getNumordre(),premiereVersionCat).get(0);
				List <Regle> listeReglePremiereVersion= premiereVersionScat.getRegles();
				assertThat(listeReglePremiereVersion).containsAll(scat.getRegles());
				//Verification de la liste de règles
				for (Regle regle: scat.getRegles()
					 ) {
					Regle derniereVersionRegle=regleRepository.findAllByNewversionIsNullAndNumordreAndSouscategorie(regle.getNumordre(),premiereVersionScat).get(0);
					assertThat(derniereVersionRegle.getId()).isEqualTo(regle.getId());
					assertThat(derniereVersionRegle.getCorpus()).isEqualTo(regle.getCorpus());
					// Verification du corpus de la règle
				}

			}
		}
	}

	@Test
	@Transactional
	void createUtilisateur(){
		Long nbrutilisateurt0= utilisateurRepository.count();
		Utilisateur utilisateur=new Utilisateur();
		String nom="Bidule";
		utilisateur.setName(nom);
		utilisateur.setPassword("truc");
		utilisateur.setRole(roleRepository.findById(1L).get());
		assertThat(utilisateur.getName()).isEqualTo("Bidule");
		assertThat(utilisateur.getId()).isEqualTo(0L);
		jpaUserService.save(utilisateur);
		assertThat(utilisateur.getId()).isNotNull();
		Long nbrutilisateurt1= utilisateurRepository.count();
		assertThat(nbrutilisateurt1).isEqualTo(nbrutilisateurt0+1);
	}

	@Test
	@Transactional
	void createCategorie() {
		Long nbCatT0= categorieRepository.count();
		// nbCatT0 le nombre d'entrée initialement dans la table categorie
		Categorie cat = new Categorie();
		//on crée un nouvelle catégorie
		cat.setTitre("Première catégorie");
		cat.setCode("X");
		// on lui donne un code (car obligatoire pour l'enregistrer dans la BDD)
		// et un titre pour pouvoir le verifier plus tard
		assertThat(cat.getId()).isNull();
		// vérification que cette cat n'a pas id
		// car elle lui donner seulement par la table ID_Generator lors de l'enregistrement
		// dans la BDD
		categorieRepository.save(cat);
		//la categorie est enregistrer via le repository
		assertThat(cat.getId()).isNotNull();
		//verification que la catgorie a bien un id maintenant
		Long nbCatT1= categorieRepository.count();
		// on compte le nombre d'entré dans la table aprés l"enregistrement
		assertThat(nbCatT1).isEqualTo(nbCatT0+1);
		//vérification que le nombre d'enregistrement a augmenter d'un enregistrement
		Categorie cat0 = categorieRepository.getOne(cat.getId());
		//on récupere va chercher cat via son id
		assertThat(cat0.getTitre()).isEqualTo("Première catégorie");
		//Verification que sont titre est bien le bon
	}

	/*@Test
	@Transactional
	void createSousCategorie() {
		assertThat(sousCategorieRepository.count()).isEqualTo(11);
		SousCategorie sousCat = new SousCategorie();
		sousCat.setTitre("Hello");
 		sousCat.setNumordre(5);
		Regle regle= new Regle();
		regle.setNumordre(1);
		sousCat.getRegles().add(regle);
		regleRepository.save(regle);
		sousCategorieRepository.save(sousCat);
		assertThat(sousCategorieRepository.count()).isEqualTo(12);
		SousCategorie scat0 = sousCategorieRepository.getOne(sousCat.getId());
		assertThat(scat0.getRegles().size()).isEqualTo(1);
	}*/

	@Test
	@Transactional
	void createRegle() {
	Long nbrRegleT0= regleRepository.count();
	Regle reglet0= new Regle();
	reglet0.setCorpus("teste de création");
	assertThat(reglet0.getId()).isNull();
	regleRepository.save(reglet0);
	assertThat(reglet0.getId()).isNotNull();
	Long nbrRegleT1= regleRepository.count();
	assertThat(nbrRegleT1).isEqualTo(nbrRegleT0+1);
	Regle reglet1= regleRepository.getOne(reglet0.getId());
	assertThat(reglet1.getCorpus()).isEqualTo("teste de création");
	}

	@Test
	@Transactional
	void constructeurCat() {
		Categorie categorie=new Categorie("Z","un test",new Date (2020,06,06),1L);
		assertThat(categorie.getCode()).isEqualTo("Z");
		assertThat(categorie.getDatemodif()).isNotNull();
		assertThat(categorieRepository.count()).isEqualTo(3);
		categorieRepository.save(categorie);
		assertThat(categorieRepository.count()).isEqualTo(4);
	}
	@Test
	@Transactional
	void constructeurSCat()
	{
		SousCategorie scat=new SousCategorie(1,"un autre test","un objet",new Date (2020,06,06,12,30,00),1L);
		assertThat(sousCategorieRepository.count()).isEqualTo(10);
		System.out.println(scat.getDatemodif().toString());
		sousCategorieRepository.save(scat);
		SousCategorie scat2=sousCategorieRepository.findById(23L).get();
		assertThat(scat2.getTitre()).isEqualTo("un autre test");
	}
	/*
	@Test
	@Transactional
	void lastvs()
	{
		Categorie catv1=new Categorie("Z","un test",new Date (2020,06,06),null);
		categorieRepository.save(catv1);

		Categorie catv2=new Categorie("Z","un deuxième test",new Date (2020,06,07),catv1.getId());
		categorieRepository.save(catv2);
		catv1.setNewversion(catv2.getId());
		categorieRepository.save(catv1);

		assertThat(categorieRepository.findById(catv2.getId()));
		//assertThat(utilitaire.nextvs(catv1)).isEqualTo(catv2);

		//Historique test1= historiqueRepository.findAllByNouvelleversionIsNullAndAncienneversion(23L);
		//System.out.println(test1 +" = "+catv2);
		//assertThat(test1).isEqualTo(catv2);

		Categorie catv3=new Categorie("Z","un dernier test",new Date (2020,06,07),catv2.getId());
		categorieRepository.save(catv3);
		catv2.setNewversion(catv3.getId());
		categorieRepository.save(catv2);
		assertThat(catv3.getNewversion()).isNull();

		assertThat(catv2.getNewversion()).isNotNull();

		//Historique test2= historiqueRepository.findAllByNouvelleversionIsNullAndAncienneversion(24L);
		List<Categorie> test3= categorieRepository.findAllByNewversionIsNullAndCode("Z");
		//assertThat(test2).isEqualTo(catv3);
		assertThat(test3.get(0)).isEqualTo(catv3);

		List<Categorie> categories= categorieRepository.containsLastVSOfRegle();
		System.out.println("catv1 = "+catv1+" catv2 = "+catv2+" catv3 = "+catv3);
		System.out.println("liste "+categories);
		for (Categorie cat: categories) {
			System.out.println(cat.getTitre());
		}
		assertThat(categories.size()).isEqualTo(2); // seulement 2 cat (A et B) les autre cat n'ont pas de sous cat et regles

		SousCategorie scatv1=new SousCategorie(1,"Titre 1","un objet",new Date (2020,06,07),null);
		scatv1.setCategorie(catv3);
		sousCategorieRepository.save(scatv1);
		catv3.getSousCategories().add(scatv1);
		categorieRepository.save(catv3);

		assertThat(scatv1.getNewversion()).isNull();
		SousCategorie scatv2=new SousCategorie(1,"Titre 1v2","un objet",new Date (2020,06,07),scatv1.getId());
		sousCategorieRepository.save(scatv2);
		catv3.getSousCategories().add(scatv2);
		categorieRepository.save(catv3);
		scatv1.setNouvelleversion(scatv2.getId());
		sousCategorieRepository.save(scatv1);

		Regle reglev1=new Regle(1,"un corpus",new Date (2020,06,07),null);
		reglev1.setSousCategorie(scatv1);
		regleRepository.save(reglev1);
		assertThat(reglev1.getNewversion()).isNull();
		scatv1.getRegles().add(reglev1);
		sousCategorieRepository.save(scatv1);
		Regle testreglev1= regleRepository.findById(reglev1.getId()).get();
		assertThat(testreglev1.getSousCategorie()).isEqualTo(scatv1);
		//assertThat(categorieRepository.findById(catv1.getId()).get().getSousCategories().size()).isEqualTo(2);
		//assertThat(categorieRepository.findById(catv3.getId()).get().getSousCategories().size()).isEqualTo(2);
		//assertThat(categorieRepository.findById(catv3.getId()).get().getSousCategories().get(1).getRegles().size()).isEqualTo(1);
		List<SousCategorie> scats= sousCategorieRepository.lastscat();
		assertThat(scats.size()).isEqualTo(7);
		List<Categorie> categories2= categorieRepository.containsLastVSOfRegle();
		System.out.println("catv1 = "+catv1+" catv2 = "+catv2+" catv3 = "+catv3);
		System.out.println("liste "+categories2);
		for (Categorie cat: categories2) {
			System.out.println(cat.getTitre());
		}

	}
	@Test
	@Transactional
	void ajoututilisateur() {
		// c'est un test
		JpaUserService JDS =new JpaUserService();
		//JDS.setGroupDao(roleRepository);
		JDS.setUserDao(utilisateurRepository);
		JDS.setbCryptPasswordEncoder(new BCryptPasswordEncoder());
		Utilisateur user1 = new Utilisateur();
		user1.setMail("editeur@test.fr");
		assertThat(roleRepository.count()).isEqualTo(3);
		List<Role> roles= (List<Role>) roleRepository.findAll();
		for (Role r: roles
			 ) {
			System.out.println("id du role ="+r.getId()+" nom du role "+r.getName());
		}
		Role admin= roleRepository.findById(3L).get();
		assertThat(admin.getName()).isEqualTo("Editeur");
		user1.setName("Prane");
		user1.setPassword("editeur");
		JDS.save(user1);
		//admin.getUsers().add(user1);
		roleRepository.save(admin);
		Utilisateur test1=utilisateurRepository.findById(1L).get();
		//assertThat(test1.getRoles().size()).isEqualTo(3);
		System.out.println(" insert into PUBLIC.UTILISATEUR (ID, ACTIVE, MAIL, NAME, PASSWORD) VALUES ("+test1.getId()+","+false+",'"+test1.getMail()+"','"+test1.getName()+"','"+test1.getPassword()+"');");
		assertThat(new BCryptPasswordEncoder().matches("editeur",test1.getPassword())).isEqualTo(true);
	}

	 */
	}




