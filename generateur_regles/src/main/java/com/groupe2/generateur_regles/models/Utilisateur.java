package com.groupe2.generateur_regles.models;

import org.jsoup.Jsoup;

import javax.persistence.*;

/**
 * <b>Utilisateur est une classe qui contient toutes les informations propre a un compte utilisateur</b>
 *
 * @author Kori,Moulin
 */
@Entity // Cette annotation indique que la classe mappe vers une table
public class Utilisateur {
    /**
     *Indique l'ID de l'utilisateur.
     */
    @Id //Cette annotation indique la corespondance entre cet attribue et la clé primaire de la table
    @GeneratedValue(strategy = GenerationType.IDENTITY)//Indique que la clé primaire est  auto-générée (ici par le SGBD)
    @Column(name = "ID", nullable = false)//Indique le nom du champs et qu'il n'acepte pas de null
    private long id;
    /**
     * Indique le nom de l'utilisateur.
     */
    @Column( nullable = false)
    private String name;
    /**
     * Le mot de passe crypté de l'utilisateur.
     */
    @Column( nullable = false)
    private String password;
    /**
     * Indique si le compte est activer ou non.
     * <b>Non implémenté</b>
     */
    private boolean active;

    /**
     * Indique l'adresse mail de l'utilisateur.
     */
    private String mail;

    /**
     * Indique le role de l'utilisateur (ici seulement un role par utilisateur)
     */
    @ManyToOne //Cette annotation indique que la table Utilisateur a une relation avec la table Role
    private Role role;

    public Utilisateur() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Jsoup.parse(name).text();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = Jsoup.parse(mail).text();
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}
