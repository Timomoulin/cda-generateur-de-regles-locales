package com.groupe2.generateur_regles.models;

import org.jsoup.Jsoup;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *<b>Categorie est une classe qui sert à regrouper les sous-catégories</b>
 *
 *En plus d'avoir ces propres attributs et méthodes, la classe Categorie hérite de la classe abstraite Historique elle hérite de ces attributs et méthodes.
 *
 * @author Kori,Moulin
 */
@Entity // Cette annotation indique que la classe mappe vers une table
public class Categorie extends Historique {

    /**
     * Le titre de la Categorie.
     */
    private String titre;
    /**
     * L'index de la Categorie.
     */
    @Column( nullable = false)
    private String code; // A B ou C
    /**
     * La liste de SousCategorie de cette Categorie.
     */
    @OneToMany(mappedBy = "categorie")
    private List<SousCategorie> souscategories = new ArrayList<SousCategorie>();

    /**
     * Un Constructeur de Categorie
     */
    public Categorie() {
        super();
    }

    /**
     * Un Constructeur de Categorie
     *
     * @param code       l'index de la Categorie en String.
     * @param titre      le titre de la Categorie en String.
     * @param datemodif  la date de création de la Categorie.
     * @param anciennevs l'id de l'ancienne version de cette Categorie en Long.
     */
    public Categorie(String code, String titre, Date datemodif, Long anciennevs) {
        super();
        this.code = Jsoup.parse(code).text();
        this.titre = Jsoup.parse(titre).text();
        this.setDatemodif(datemodif);
        this.setExversion(anciennevs);
    }
   //Acceseurs et Mutateur

    /**
     * Accesseur de l'attribut souscategories. Retourne la liste de SousCategorie de cette Categorie.
     *
     * @return La liste de SousCategorie de cette Categorie.
     */
    public List<SousCategorie> getSousCategories() {
        return souscategories;
    }

    /**
     * Mutateur de l'attribut souscategorie.
     *
     * @param souscategories La liste de SousCategorie de cette Categorie.
     */
    public void setSousCategories(List<SousCategorie> souscategories) {
        this.souscategories = souscategories;
    }

    /**
     *Accesseur de l'attribut code. Retourne le code de la Categorie.
     *
     * @return Une String qui represente l'index de la Categorie.
     */
    public String getCode() {
        return code;
    }

    /**
     * Mutateur de l'attribut code.
     *
     * @param code Une String qui represente l'index de la Categorie.
     */
    public void setCode(String code) {
        this.code = Jsoup.parse(code).text();
    }


    /**
     * Accesseur de l'attribut titre. Retourne le titre de la Categorie.
     *
     * @return Une String qui represente le titre de la Categorie.
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Mutateur de l'attribut titre.
     *
     * @param title Une String qui represente le titre de la Categorie.
     */
    public void setTitre(String title) {
        this.titre = Jsoup.parse(title).text();
    }

}
