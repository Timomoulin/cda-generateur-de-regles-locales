package com.groupe2.generateur_regles.models;

import javax.persistence.*;
import java.util.Set;

/**
 * <b>Role est une classe qui représente les différents role possible pour un compte utilisateur</b>
 */
@Entity // Cette annotation indique que la classe mappe vers une table
public class Role {
    /**
     * Indique l'ID de l'utilisateur.
     */
    @Id //Cette annotation indique la corespondance entre cet attribue et la clé primaire de la table
    @GeneratedValue(strategy = GenerationType.IDENTITY)//Indique que la clé primaire est  auto-générée (ici par le SGBD)
    @Column(name = "ID", nullable = false)//Indique le nom du champs et qu'il n'acepte pas de null
    private long id;
    /**
     * Indique le nom du role.
     */
    private String name;
    /**
     * Indique la valeur qui serat indiquer dans l'attribut authorities de la classe User de Spring Security
     */
    private String role;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
    /*public Set<Utilisateur> getUsers() {
        return users;
    }

    public void setUsers(Set<Utilisateur> users) {
        this.users = users;
    }

     */

