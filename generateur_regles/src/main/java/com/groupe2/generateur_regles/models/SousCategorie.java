package com.groupe2.generateur_regles.models;
import org.jsoup.Jsoup;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <b>SousCategorie est une classe qui sert à regrouper les règles locales</b>
 *
 * En plus d'avoir ces propres attributs et méthodes, la classe SousCategorie hérite de la classe abstraite Historique elle hérite de ces attributs et méthodes.
 *
 *@author Kori,Moulin
 *
 */
@Entity // Cette annotation indique que la classe mappe vers une table
public class SousCategorie extends Historique {

    /**
     * L'index de la SousCategorie.
     */
    private Integer numordre;
    /**
     * Le titre de la SousCategorie.
     */
    private String titre;
    /**
     * L'objet de la SousCategorie qui introduit le contexte des règles locales.
     */
    @Column(length = 20000)
    private String objet;
    /**
     * La Categorie à laquelle appartient cette SousCategorie.
     */
    @ManyToOne
    private Categorie categorie ;
    /**
     * Les règles contenues dans cette SousCategorie.
     */
    @OneToMany
            (mappedBy="souscategorie")
    private List<Regle> regles=new ArrayList<>() ;

    /**
     * Un constructeur de SousCategorie.
     */
    public SousCategorie() {
        super();
    }

    /**
     * Un constructeur de SousCategorie.
     *
     * @param numordre L'index de la SousCategorie en Integer.
     * @param titre Le titre de la SousCategorie en String.
     * @param objet L'objet de la SousCategorie qui introduit le contexte des règles locales en String.
     * @param datemodif La date de création de cette SousCategorie.
     * @param anciennevs @param anciennevs l'id de l'ancienne version de cette SousCategorie en Long.
     */
    public SousCategorie(Integer numordre, String titre, String objet, Date datemodif, Long anciennevs) {
        super();
        this.numordre=numordre;
        this.titre = Jsoup.parse(titre).text();
        this.objet= Jsoup.parse(objet).text();
        this.setDatemodif(datemodif);
        this.setExversion(anciennevs);
    }

    /**
     *  Accesseur de l'attribut categorie. Retourne une Categorie.
     *
     * @return l'objet de la classe Categorie auxquelles cette SousCategorie appartient.
     */
    public Categorie getCategorie() {
        return categorie;
    }

    /**
     * Mutateur de l'attribut categorie.
     *
     * @param categorie La Categorie auxquelles la SousCategorie appartient.
     */
    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    /**
     * Accesseur de l'attribut numordre. Retourne un Integer.
     *
     * @return L'index de la SousCategorie en Integer.
     */
    @Column( nullable = false)
    public Integer getNumordre() {
        return numordre;
    }

    /**
     * Mutateur de l'attribut numordre.
     *
     * @param ordre L'index de la SousCategorie en Entier.
     */
    public void setNumordre(Integer ordre) {
        this.numordre = ordre;
    }

    /**
     * Accesseur de l'attribut titre. Retourne le titre en String.
     *
     * @return Le titre de la SousCategorie en String.
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Mutateur de l'attribut Titre.
     *
     * @param title Le titre de la SousCategorie en String.
     */
    public void setTitre(String title) {
        this.titre = Jsoup.parse(title).text();
    }

    /**
     * Accesseur de l'attribut objet. Retourne l'objet en String
     *
     * @return l'objet de la SousCategorie en String.
     */
    public String getObjet() {
        return objet;
    }

    /**
     * Mutateur de l'attribut objet qui parse le HTML.
     *
     * @param objet l'objet de la SousCategorie en String.
     */
    public void setObjet(String objet) {
        this.objet = Jsoup.parse(objet).text();
    }

    /**
     * Mutateur de l'attribut objet ne parse pas l'objet
     *
     * @param objet l'objet de la SousCategorie en String.
     */
    public void setObjetBB2(String objet) { this.objet = objet; }
    /**
     * Acceseur de l'attribut regles. Retroune une liste de Regle.
     *
     * @return La liste d'objet de la classe Regle appartenant à la SousCategorie
     */
    public List<Regle> getRegles() {
        return regles;
    }

    public void setRegles(List<Regle> regles) {
        this.regles = regles;
    }
}


