package com.groupe2.generateur_regles.models;

import org.jsoup.Jsoup;

import javax.persistence.*;
import java.util.Date;
/**
 * <b>Regle est une classe qui représente un modèle de règle locale</b>
 *
 * En plus d'avoir ces propres attributs et méthodes, la classe Regle hérite de la classe abstraite Historique elle hérite de ces attributs et méthodes.
 *
 *@author Kori,Moulin
 *
 */
@Entity // Cette annotation indique que la classe mappe vers une table
public class Regle extends Historique {

    /**
     * L'index d'une Regle.
     */
    @Column( nullable = false)
    private Integer numordre;
    /**
     * Le corps de la règle locale.
     */
    @Column(length = 20000)
    private String corpus;
    /**
     * La SousCategorie à laquelle appartient cette Regle.
     */
   @ManyToOne
    SousCategorie souscategorie;

    /**
     * Un Constructeur de Regle.
     */
    public Regle() {
        super();
    }

    /**
     * Un Constructeur de Regle.
     *
     * @param numordre L'index de la Regle en Integer.
     * @param corpus Le corps de la Regle en String.
     * @param datemodif La date de création de la Regle.
     * @param anciennevs l'id de l'ancienne version de cette Regle en Long.
     */
    public Regle(Integer numordre, String corpus, Date datemodif,Long anciennevs) {
        super();
        this.numordre=numordre;
        this.corpus=Jsoup.parse(corpus).text();
        this.setDatemodif(datemodif);
        this.setExversion(anciennevs);
    }

    /**
     * Un Constructeur de Regle.
     *
     * @param scat La SousCategorie de la Regle.
     * @param corpus Le corps de la Regle en String.
     * @param numordre L'index de la Regle en Integer.
     */
    public Regle(SousCategorie scat, String corpus, Integer numordre)
    {
        this.souscategorie = scat;
        this.corpus = Jsoup.parse(corpus).text();
        this.numordre = numordre;
    }

   /* public Regle(String txt) {
        this.setCorpus(txt);
    }*/


    /**
     * Accesseur de l'attribut numordre retourne l'index de la Regle .
     *
     * @return L'index de la Regle en Integer.
     */
    public Integer getNumordre() {
        return numordre;
    }

    /**
     * Mutateur de l'attribut numordre.
     *
     * @param ordre L'index de la Regle en Integer.
     */
    public void setNumordre(Integer ordre) {
        this.numordre = ordre;
    }


    /**
     * Accesseur de l'attribut corpus retourne le corps de la Regle.
     *
     * @return Le corpus de la Regle en String.
     */
    public String getCorpus() {
        return corpus;
    }

    /**
     * Mutateur de l'attribut corpus.
     *
     * @param corpus Le corpus de la Regle en String.
     */
    public void setCorpus(String corpus) { this.corpus = Jsoup.parse(corpus).text(); }
    public void setCorpusBB2(String corpus) { this.corpus = corpus; }
    /**
     * Acceseur de l'attribut souscategorie retourne la SousCategorie de la Regle.
     *
     * @return L'objet de la classe SousCategorie auxqelles appartient la Regle.
     */
    public SousCategorie getSousCategorie() {
        return souscategorie;
    }

    /**
     * Mutateur de l'attribut souscategorie.
     *
     * @param sousCategorie L'objet de la classe SousCategorie auxqelles appartient la Regle.
     */
    public void setSousCategorie(SousCategorie sousCategorie) {
        this.souscategorie = sousCategorie;
    }

    /**
     * Permet d'obtenir la Categorie auxquelle la Regle appartient.
     *
     * @return L'objet de la classe Categorie auxquelles la Regle appartient
     */
    public  Categorie getCategorie(){
        return this.getSousCategorie().getCategorie();
    }
    /**
     * Permets d'obtenir l'index complet de la Regle en concaténant les index de la Categorie, SousCategorie, et la Regle.
     *
     * @return l'index complet de la Regle en String.
     */
    public String getCode() {
        return this.getSousCategorie().getCategorie().getCode() + '.' + this.getSousCategorie().getNumordre() + '.' + this.getNumordre();

    }

}
