package com.groupe2.generateur_regles.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

import static javax.persistence.InheritanceType.SINGLE_TABLE;
import static javax.persistence.InheritanceType.TABLE_PER_CLASS;
@Entity
@Inheritance(strategy= TABLE_PER_CLASS) //Cette annotation indique comment on souhaite implémenter l'héritage dans la BDD
abstract public class Historique   {
    @Id //Cette annotation indique la correspondance entre cet attribue et la clé primaire de la table
    @TableGenerator(
            name = "HISTORIQUE_GEN",
            table = "ID_Generator", //nom de la table qui va gerer la clé primaire
            pkColumnName = "name", //indique le nom de la CP
            valueColumnName = "sequence", // indique la dernière valeur de la CP
            initialValue = 26, //Valeur initiale de la CP
            allocationSize = 1)//indique de combien on incrémente la CP
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "HISTORIQUE_GEN")
    //Indique que la clé primaire est  auto-générée (ici via la création d'une table "ID_Generator")

    @Column(name = "ID")
    private Long id; //cle primaire des cat,scat,regle
    /**
     * Indique l'id de la version précédente (peut être null).
     */
    private Long exversion;
    /**
     * Indique l'id de la version suivante (peut être null).
     */
    private Long newversion;
    /**
     * Indique la date de création de cette version (Non implémenté).
     */
    private Date datemodif;
    /**
     * Indique l'auteur de cette version (Non implémenté).
     */
    private Long auteur;
    /**
     * Un constructeur d'Historique
     */
    public Historique() {
    }
    /**
     * Permets de transférer certains attributs d'une version à une autre .
     *
     * @param h La version dont on veut importer les attributs .
     */
    public void tranforme (Historique h)
    {
        this.setNewversion(h.getNewversion());
        this.setExversion(h.getExversion());
        this.setAuteur(h.getAuteur());
        this.setDatemodif(h.getDatemodif());
        this.setId(h.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Historique that = (Historique) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExversion() {
        return exversion;
    }

    public void setExversion(Long ancinneversion) {
        this.exversion = ancinneversion;
    }

    public Long getNewversion() {
        return newversion;
    }

    public void setNewversion(Long nouvelleversion) {
        this.newversion = nouvelleversion;
    }

    public Date getDatemodif() {
        return datemodif;
    }

    public void setDatemodif(Date datemodif) {
        this.datemodif = datemodif;
    }

    public Long getAuteur() {
        return auteur;
    }

    public void setAuteur(Long autheur) {
        this.auteur = autheur;
    }


}
