package com.groupe2.generateur_regles.services;

import com.groupe2.generateur_regles.repository.CategorieRepository;
import com.groupe2.generateur_regles.repository.RegleRepository;
import com.groupe2.generateur_regles.repository.SousCategorieRepository;
import com.groupe2.generateur_regles.models.*;
import org.primeframework.transformer.domain.Document;
import org.primeframework.transformer.service.BBCodeParser;
import org.primeframework.transformer.service.BBCodeToHTMLTransformer;
import org.primeframework.transformer.service.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class GenerateurService {
    @Autowired
    private CategorieRepository categorieRepository;
    @Autowired
    private SousCategorieRepository sousCategorieRepository;
    @Autowired
    private RegleRepository regleRepository;


    public void setCategorieRepository(CategorieRepository categorieRepository) {
        this.categorieRepository = categorieRepository;
    }

    public void setSousCategorieRepository(SousCategorieRepository sousCategorieRepository) {
        this.sousCategorieRepository = sousCategorieRepository;
    }

    public void setRegleRepository(RegleRepository regleRepository) {
        this.regleRepository = regleRepository;
    }

    /**
     * Permet de convertir des balises BBcode en balise HTML
     *
     * @param input Une chaine avec des balises BBcode
     * @return Une chaine avec des balises HTML
     */
    private String convertirBBcodeEnHTML(String input) {
        Document document = new BBCodeParser().buildDocument(input, null);
        String html = new BBCodeToHTMLTransformer().transform((org.primeframework.transformer.domain.Document) document, (node) -> {
            // transform predicate, returning false will cause this node to not be transformed
            return true;
        }, new Transformer.TransformFunction.HTMLTransformFunction(), null);
        return html;
    }

    /* Permet de trier une liste de cat dans l'ordre (code)
     *@Param Une liste de categories
     * @Return Une liste de categories trier dans l'ordre
     */
    public List<Categorie> classeCategories(List<Categorie> categories) {
        List<Categorie> resultat = new ArrayList<>();
        String chaine = "ABCDEFGHIJKLMNOPQRSTUVWXYZ?";
        for (char lettre : chaine.toCharArray()) {
            for (Categorie categorie : categories) {
                if (categorie.getCode().charAt(0) == lettre) {
                    Comparator<SousCategorie> compareScatByNum =
                            (SousCategorie scat1, SousCategorie scat2) -> scat1.getNumordre().compareTo(scat2.getNumordre());
                    categorie.getSousCategories().sort(compareScatByNum);
                    for (SousCategorie scat : categorie.getSousCategories()) {
                        Comparator<Regle> compareRegleByNum =
                                (Regle regle1, Regle regle2) -> regle1.getNumordre().compareTo(regle2.getNumordre());
                        scat.getRegles().sort(compareRegleByNum);
                    }
                    resultat.add(categorie);
                }
            }
        }
        return resultat;
    }

    public List<Categorie> lastvs() {
        List<Categorie> toutesCat = categorieRepository.findAll();
        List<Categorie> resultat = new ArrayList<Categorie>();

        for (Categorie c : toutesCat) {
            Categorie cat = new Categorie(c.getCode(), c.getTitre(), c.getDatemodif(), c.getExversion());
            if (c.getNewversion() == null) {
                cat.tranforme(c);
                resultat.add(cat);
            }
            Categorie v1 =
                    categorieRepository.findAllByExversionIsNullAndCode(c.getCode()).get(0);
            if (v1.getSousCategories() != null) {
                for (SousCategorie sc : v1.getSousCategories()
                ) {

                    SousCategorie scat =
                            new SousCategorie(sc.getNumordre(), sc.getTitre(), sc.getObjet(), sc.getDatemodif(), sc.getExversion());
                    String source = scat.getObjet();

                    scat.setObjetBB2(this.convertirBBcodeEnHTML(scat.getObjet()));
                    if (sc.getNewversion() == null) {
                        scat.tranforme(sc);
                        scat.setCategorie(cat);
                        cat.getSousCategories().add(scat);
                    }
                    SousCategorie scv1 =
                            sousCategorieRepository.findAllByExversionIsNullAndNumordreAndCategorie(scat.getNumordre(), sc.getCategorie()).get(0);
                    if (scv1.getRegles() != null) {
                        for (Regle r : scv1.getRegles()) {
                            if (r.getNewversion() == null) {
                                Regle regle = new Regle();
                                regle.tranforme(r);
                                regle.setNumordre(r.getNumordre());
                                regle.setCorpusBB2(this.convertirBBcodeEnHTML(r.getCorpus()));
                                regle.setSousCategorie(scat);
                                scat.getRegles().add(regle);
                            }
                        }
                    }
                }
            }
        }
        return this.classeCategories(resultat);
    }



    public List<Categorie> listeCatDesRegles(List<Regle> regles) {
        List<Categorie> categories = new ArrayList<Categorie>();
        for (Regle regle : regles
        ) {
            Boolean contienCat = false;
            Boolean contientSCat = false;
            Categorie categorieRegle = new Categorie();
            SousCategorie souscatRegle = new SousCategorie();
            regle.setCorpusBB2(this.convertirBBcodeEnHTML(regle.getCorpus()));
            for (Categorie categorie : categories
            ) {
                if (categorie.getCode().equals(regle.getSousCategorie().getCategorie().getCode())
                ) {
                    contienCat = true;
                    categorieRegle = categorie;

                    for (SousCategorie scat : categorie.getSousCategories()
                    ) {
                        scat.setObjetBB2(this.convertirBBcodeEnHTML(scat.getObjet()));
                        if (scat.getNumordre().equals(regle.getSousCategorie().getNumordre())) {
                            contientSCat = true;
                            souscatRegle = scat;
                        }
                        for (Regle r : scat.getRegles()
                        ) {
                            r.setCorpusBB2(this.convertirBBcodeEnHTML(r.getCorpus()));
                        }
                    }
                }
            }
            if (contienCat == false) {
                Categorie cat = new Categorie();
                Categorie catlastvs =
                        categorieRepository.findAllByNewversionIsNullAndCode(regle.getSousCategorie().getCategorie().getCode()).get(0);
                SousCategorie scatlastvs =
                        sousCategorieRepository.findAllByNewversionIsNullAndNumordreAndCategorie(regle.getSousCategorie().getNumordre(), regle.getSousCategorie().getCategorie()).get(0);
                cat.setTitre(catlastvs.getTitre());
                cat.setCode(catlastvs.getCode());
                cat.setId(regle.getSousCategorie().getCategorie().getId());
                SousCategorie scat = new SousCategorie();
                scat.getRegles().add(regle);
                scat.setTitre(scatlastvs.getTitre());
                scat.setNumordre(scatlastvs.getNumordre());
                scat.setObjet(scatlastvs.getObjet());
                cat.getSousCategories().add(scat);
                categories.add(cat);
            } else if (contienCat && contientSCat == false) {
                SousCategorie scat = new SousCategorie();
                SousCategorie scatlastvs =
                        sousCategorieRepository.findAllByNewversionIsNullAndNumordreAndCategorie(regle.getSousCategorie().getNumordre(), regle.getSousCategorie().getCategorie()).get(0);

                scat.getRegles().add(regle);
                scat.setTitre(scatlastvs.getTitre());
                scat.setNumordre(scatlastvs.getNumordre());
                scat.setObjet(scatlastvs.getObjet());
                categorieRegle.getSousCategories().add(scat);
            } else if (contienCat && contientSCat) {
                souscatRegle.getRegles().add(regle);
            }
        }
        categories = this.classeCategories(categories);
        return categories;
    }


}
