package com.groupe2.generateur_regles.services;

import com.groupe2.generateur_regles.models.Utilisateur;
import com.groupe2.generateur_regles.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class JpaUserService {
    @Autowired
    private UtilisateurRepository userDao;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void setUserDao(UtilisateurRepository userDao) {
        this.userDao = userDao;
    }

    public JpaUserService() {
        this.bCryptPasswordEncoder = new BCryptPasswordEncoder();
    }

    public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void save(Utilisateur user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userDao.save(user);
    }

    public Utilisateur findByUserName(String userName) {
        return userDao.findByName(userName);
    }
}
