package com.groupe2.generateur_regles.security.configuration;

import com.groupe2.generateur_regles.services.JpaUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private JpaUserDetailsService jpaUserDetailsService;
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public void setUserDetailsService(JpaUserDetailsService jpaUserDetailsService){
        this.jpaUserDetailsService = jpaUserDetailsService;
    }

    @Bean
    public PasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //.csrf().disable()
                //par défaut cross site request forgery est activer
                //ce qui est une bonne chose car cela couvre une faille de sécurité importante
                //mais cela veut dire que on doit joindre une clé csrf dans tous les formulaires POST
                .authorizeRequests()
                .antMatchers("/", "/home","/Arbitre/**").permitAll()
                //ce sont les URL qui on pas besoin d'autorisation
                .antMatchers("/Admin/**").hasAuthority("ADMIN")
                //les url seulement si l'utilisateur a l'autorisation ADMIN
                // = si l'utilisateur a le role  d'Admin
                .antMatchers("/Gestionnaire/**").hasAnyAuthority("ADMIN","GESTIONNAIRE")
                // si l'utilisateur est un admin ou gestionnaire
                .antMatchers("/Editeur/**").hasAnyAuthority("ADMIN","GESTIONNAIRE","EDITEUR")
                // si l'utilisateur est un admin ou gestionnaire ou editeu
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                //l'url de la page de connexion
                .loginProcessingUrl("/login")
                // l'url a laquelle la requete identification est envoyer
                .defaultSuccessUrl("/Arbitre/Categories", true)
                .failureUrl("/Arbitre/Categories")
                // on redige l'utilisateur sur la même page
                // en si l'utilisateur a réussi son identification  ou pas
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                // l'url de la page de deconnexion
                .logoutSuccessUrl("/Arbitre/Categories")
                .permitAll();
    }


   /* @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
       auth.inMemoryAuthentication()
                .withUser("recup")
                .password(bCryptPasswordEncoder().encode("recup"))
                .authorities("WITHDRAW","DEPOSIT","USER","ADMIN","ROLE_ADMIN")
        ;
        //auth.userDetailsService(jpaUserDetailsService).passwordEncoder(bCryptPasswordEncoder());
        //System.out.println("Password : "+bCryptPasswordEncoder().encode("passe"));
    }*/

}