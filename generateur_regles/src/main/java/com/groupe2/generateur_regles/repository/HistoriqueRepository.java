package com.groupe2.generateur_regles.repository;

import com.groupe2.generateur_regles.models.Historique;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoriqueRepository extends JpaRepository <Historique,Long> {
}
