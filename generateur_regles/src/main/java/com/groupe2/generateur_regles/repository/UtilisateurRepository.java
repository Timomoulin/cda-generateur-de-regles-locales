package com.groupe2.generateur_regles.repository;

import com.groupe2.generateur_regles.models.Utilisateur;
import org.springframework.data.repository.CrudRepository;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long> {
   Utilisateur findByName(String name);
}
