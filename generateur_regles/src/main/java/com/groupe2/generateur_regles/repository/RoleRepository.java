package com.groupe2.generateur_regles.repository;

import com.groupe2.generateur_regles.models.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}
