package com.groupe2.generateur_regles.repository;
import com.groupe2.generateur_regles.models.Categorie;
import com.groupe2.generateur_regles.models.SousCategorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
/**
 *SousCategorieRepository est un interface qui herite de JpaRepository.
 */
public interface SousCategorieRepository extends JpaRepository<SousCategorie, Long> {
    List<SousCategorie> findAllByCategorie(Categorie categories);
    List<SousCategorie> findAllByNewversionIsNullAndNumordreAndCategorie(Integer numordre, Categorie categorie);
    List<SousCategorie> findAllByExversionIsNullAndNumordreAndCategorie(Integer numordre, Categorie categorie);
    List<SousCategorie> findAllByNumordreAndCategorie(Integer numordre,Categorie categorie);

}
    /*@Query(value = "select distinct * from SOUS_CATEGORIE where ID in (select SOUSCATEGORIE_ID from REGLE where REGLE.NOUVELLEVERSION is null)",nativeQuery = true)
    List<SousCategorie> lastscat ();*/