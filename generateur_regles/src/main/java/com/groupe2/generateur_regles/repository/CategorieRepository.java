package com.groupe2.generateur_regles.repository;
import com.groupe2.generateur_regles.models.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *CategorieRepository est un interface qui herite de JpaRepository.
 */
public interface CategorieRepository extends JpaRepository<Categorie, Long>
{
    /**
     * Une méthode qui interoge la BDD retourne la dernière version d'une Categorie dans une    liste.
     *
     * @param code Le code d'une categorie.
     * @return Une liste de Categorie qui on ce code et qui sont les versions les plus récentes.
     */
    List<Categorie> findAllByNewversionIsNullAndCode(String code);

    /**
     * Une méthode qui interoge la BDD est retourne la première version d'une Categorie dans une liste.
     *
     * @param code Le code d'une categorie.
     * @return Une liste de Categorie qui on ce code et qui sont les versions les plus anciennes.
     */
    List<Categorie> findAllByExversionIsNullAndCode(String code);

    /**
     * Une méthode qui interoge la BDD est retourne une liste de toutes les versions d'une Categorie.
     *
     * @param code Le code d'une categorie.
     * @return Une liste de Categorie qui on ce code.
     */
    List<Categorie> findAllByCode(String code);
}
