package com.groupe2.generateur_regles.repository;
import com.groupe2.generateur_regles.models.Regle;
import com.groupe2.generateur_regles.models.SousCategorie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegleRepository extends JpaRepository<Regle, Long>{
    List<Regle> findAllBySouscategorie(SousCategorie sousCategorie);
    List<Regle> findAllByNumordreAndSouscategorie(Integer numordre, SousCategorie souscategorie);
    List<Regle> findAllByNewversionIsNullAndNumordreAndSouscategorie(Integer numordre,SousCategorie sousCategorie);
    List<Regle> findAllByExversionIsNullAndNumordreAndSouscategorie(Integer numordre,SousCategorie sousCategorie);
}
