package com.groupe2.generateur_regles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenerateurReglesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenerateurReglesApplication.class, args);
	}

}
