package com.groupe2.generateur_regles.controllers;

import com.groupe2.generateur_regles.repository.*;
import com.groupe2.generateur_regles.models.*;
import com.groupe2.generateur_regles.services.JpaUserService;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller //Cette annotation indique que cette classe joue le role d'un Controleur
@RequestMapping(value = "/Editeur/") //Cette annotation indique que tout URI utilisé par les méthodes
// commence par /Editeur
public class EditeurControleur {

    @Autowired //Cette annotation indique un point d'injection d'un bean
    private SousCategorieRepository sousCategorieRepository; // On met en place le repository sans utilisé de setter
    @Autowired
    private RegleRepository regleRepository;

    /**
     * vers le formulaire de modification d'une sous-catégorie
     *
     * @param model Permet de transmettre des attributs au template
     * @param id    L'id d'une Sous Categorie transmit dans l'URL.
     * @return le nom du template qui est utilisé pour le rendu et auquel on communique le model.
     */
    @RequestMapping(value = "/SousCategorie/modif/{id}", method = RequestMethod.GET)//Cette annotations indique
    // l'url mapper par cette méthode, de même que la méhtode HTTP utilisé ici GET
    public String modifsouscategorie(Model model, @PathVariable("id") Long id) {
        SousCategorie scat = sousCategorieRepository.findById(id).get();
        // on utilise la méthode findById pour  à partir de l’id instancier notre objet
        //SousCategorie
        model.addAttribute("scat", scat);
        return "modifsouscat";
    }

    @RequestMapping(value = "/SousCategorie/modif", method = RequestMethod.POST)
    public String modifsouscat(Model model, @RequestParam("id") Long id, @RequestParam("numordre") Integer numordre, @RequestParam("title") String title, @RequestParam("object") String object) {
        SousCategorie exVSScat = sousCategorieRepository.findById(id).get();
        SousCategorie nouvelleVSScat = new SousCategorie();
        nouvelleVSScat.setExversion(id); //j'indique l'ancienne version de cette scat
        nouvelleVSScat.setObjet(object);//l'objet est parser dans le mutateur
        nouvelleVSScat.setTitre(title);// le titre est parser dans le mutateur
        nouvelleVSScat.setNumordre(numordre);
        nouvelleVSScat.setCategorie(exVSScat.getCategorie());
        //TODO Utiliser les setAutheur, setDatemodif
        sousCategorieRepository.save(nouvelleVSScat); // je sauvegarde dans la bdd la nouvelle version de cette scat
        exVSScat.setNewversion(nouvelleVSScat.getId()); // j'indique que il existe desormé une nouvelle version de cette version
        sousCategorieRepository.save(exVSScat); // je sauvegarde cette ex vs dans la bdd
        return "redirect:/Arbitre/Categories";
    }


    // vers le formulaire de modification des règles
    @RequestMapping(value = "/Regle/modif/{id}", method = RequestMethod.GET)
    public String modifrgl(Model model, @PathVariable("id") Long id) {
        Regle rgl = regleRepository.findById(id).get();
        model.addAttribute("rgl", rgl);
        return "modifregle";
    }

    // un deuxieme qui recupère les infos en post et créer une nvl version de cette règle (regarder dans controleur editeur comment faire une modif)
    @RequestMapping(value = "/Regle/modif", method = RequestMethod.POST)
    public String modifregle(Model model, @RequestParam("numordre") Integer numordre, @RequestParam("id") Long id, @RequestParam("corpus") String corpus) {
        Regle exrgl = regleRepository.findById(id).get();
        Regle regle = new Regle(exrgl.getSousCategorie(), corpus, numordre);
        regle.setExversion(id);
        regleRepository.save(regle);
        exrgl.setNewversion(regle.getId());
        regleRepository.save(exrgl);
        return "redirect:/Arbitre/Categories";
    }


}
