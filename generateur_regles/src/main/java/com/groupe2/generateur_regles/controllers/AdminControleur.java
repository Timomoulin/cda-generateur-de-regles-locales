package com.groupe2.generateur_regles.controllers;

import com.groupe2.generateur_regles.repository.*;
import com.groupe2.generateur_regles.models.*;
import com.groupe2.generateur_regles.services.JpaUserService;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(value = "/Admin/")
public class AdminControleur {
    @Autowired
    private JpaUserService jpaUserService;
    @Autowired
    private HistoriqueRepository historiqueRepository;
    @Autowired
    private CategorieRepository categorieRepository;
    @Autowired
    private SousCategorieRepository sousCategorieRepository;
    @Autowired
    private RegleRepository regleRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private RoleRepository roleRepository;

    // supprimer une categorie
    @RequestMapping(value = "/Categorie/supp/{id}", method = RequestMethod.GET)
    public String suppcategorie(Model model, @PathVariable("id") Long id) {
        Categorie cat = categorieRepository.findById(id).get();
        List<Categorie> toutesVersions = categorieRepository.findAllByCode(cat.getCode());
        for (Categorie parcoursCat : toutesVersions) {
            List<SousCategorie> scats = sousCategorieRepository.findAllByCategorie(parcoursCat);
            if (scats.size() > 0) {
                for (SousCategorie scat : scats) {
                    List<Regle> regles = regleRepository.findAllBySouscategorie(scat);
                    if (regles.size() > 0) {
                        for (Regle regle : regles) {
                            regleRepository.delete(regle);
                        }
                    }
                    sousCategorieRepository.delete(scat);
                }
            }
            categorieRepository.delete(parcoursCat);
        }
        return "redirect:/Arbitre/Categories";
    }

    //supprime une sous categorie
    @RequestMapping(value = "/SousCategorie/supp/{id}", method = RequestMethod.GET)
    public String suppsouscategorie(Model model, @PathVariable("id") Long id) {
        SousCategorie scat = sousCategorieRepository.findById(id).get();
        //on récupére la sous-catégorie a supprimé dans la BDD grace à son id
        List<Regle> regles = regleRepository.findAllBySouscategorie(scat);
        //on récupére la liste de règles (depuis la BDD) qui appartient à cette sous-catégorie
        List<SousCategorie> scatdecale = sousCategorieRepository.findAllByCategorie(scat.getCategorie());
        for (SousCategorie sc : scatdecale
        ) {
            System.out.println(sc.getId());
            if (sc.getNumordre() > scat.getNumordre()) {
                System.out.println("///////");
                sc.getNumordre();
                sc.setNumordre(sc.getNumordre() - 1);
                sc.getNumordre();
            }
            if (regles.size() > 0)
            // si la liste de règle n'est pas vide
            {
                for (Regle regle : regles)
                // alors on parcourt la liste de règles
                {
                    regleRepository.delete(regle);
                    //et on supprime chaque regle dans la liste
                }
            }
            sousCategorieRepository.delete(scat);


        }
        // on utilise le repository pour supprimer la sous-catégorie dans la BDD
        return "redirect:/Arbitre/Categories";
        //On utilise un redirect vers la page principal
    }

    // supprimer une regle
    @RequestMapping(value = "/Regle/supp/{id}", method = RequestMethod.GET)
    public String suppregle(Model model, @PathVariable("id") Long id) {
        Regle reg = regleRepository.findById(id).get();
        List<Regle> decalage = regleRepository.findAllBySouscategorie(reg.getSousCategorie());
        List<Regle> regles = regleRepository.findAllByNumordreAndSouscategorie(reg.getNumordre(), reg.getSousCategorie());

        regleRepository.deleteAll(regles);
        for (Regle r : decalage
        ) {
            if (r.getNumordre() > reg.getNumordre()) {
                r.setNumordre(r.getNumordre() - 1);
                regleRepository.save(r);
            }
        }

        return "redirect:/Arbitre/Categories";
    }

    // vers le formulaire d'inscription
    @RequestMapping(value = "/inscription/")
    public String inscription(Model model) {
        List<Role> roles = (List<Role>) roleRepository.findAll();
        //on utilise le roleRepository pour interroger la BDD
        // et crée une liste de tous les rôles existant dans la table Role
        model.addAttribute("roles", roles);
        //on ajoute un attribut roles au modèle qui pour valeur la liste de rôles
        // via le modèle cet attribut serat communiquer au template
        return "inscription";
    }

   /* @RequestMapping(value = "/Admin/Inscriptions", method = RequestMethod.POST)
    public String inscritUtilisateur(Model model, @RequestParam("name") String n0, @RequestParam("email") String m1,
                                     @RequestParam("psw") String m2, @RequestParam("psw-repeat") String m3) {

        JpaUserService insc = new JpaUserService();
        insc.setGroupDao(groupRepository);
        insc.setUserDao(userRepository);
        insc.setbCryptPasswordEncoder(new BCryptPasswordEncoder());
        User userX = new User();
        userX.setName(n0);
        userX.setMail(m1);
        userX.setPassword(m2);

        char i;
        String msg = "Le mot de passe ne rempli pas les demandes exigées";
        // 1ere vérification : égalité des mots de passes
        if (m2.length() >= 7 && m2.equals(m3)) {
            // Les vérifications suivante concerne la forme des caractères(Maj, Min, ou Digits)
            int isUp = 0;
            int isLow = 0;
            int isDgt = 0;
            for (i = 0; i < m2.length(); i++) {
                if (Character.isUpperCase(m2.charAt(i))) {
                    isUp++;
                } else if (Character.isLowerCase(m2.charAt(i))) {
                    isLow++;
                } else if (Character.isDigit(m2.charAt(i))) {
                    isDgt++;
                }
            }
            if (isUp > 0 && isLow > 0 && isDgt > 0) {

                insc.save(userX);
                model.addAttribute("user", userRepository.findAll());

                return "redirect:/Regles";
                // TODO Il faut ajouter la condition sur les caractères spéciaux
            } else {
                String msg = "Le mot de passe ne rempli pas les demandes exigées";
                // System.out.println("Le mot de passe ne rempli pas les demandes exigées");
                model.addAttribute("msg", msg);
                return "Inscription";
            }

        } else {
            String msg = "Le mot de passe ne rempli pas les demandes exigées";
            // System.out.println("Le mot de passe ne rempli pas les demandes exigées");
            model.addAttribute("msg", msg);
            return "Inscription";
        }
        return "redirect:/Admin/Inscriptions";
    }*/

    //recuperation du formulaire d'inscription
    @RequestMapping(value = "/inscription/", method = RequestMethod.POST)
    public String inscription(Model model, @RequestParam("nom") String usernom, @RequestParam("mail") String usermail, @RequestParam("mdp") String mdp, @RequestParam("mdp2") String mdp2, @RequestParam("idrole") Long idrole) {
        //Note : Seulement l'Administrateur peut inscrire un utilisateur.
        //TODO Verification du la double saisie de mdp et doit conetenir chriffre nombre et un char spécial
        //TODO Meilleur validation
        String usermdp = Jsoup.parse(mdp).text(); //on parse le mdp avant l'encryption
        // Au cas ou l'encryption gènere alétoirement une balise HTML.
        if (usernom.isEmpty() == false && usermail.isEmpty() == false && mdp.isEmpty() == false && mdp.equals(mdp2) && utilisateurRepository.findByName(usernom) == null) {
            //validation de nos RequestParam
            Utilisateur user = new Utilisateur();
            user.setName(usernom); // Ce mutateur parse le nom.
            user.setMail(usermail); // Ce mutateur parse le mail.
            user.setPassword(usermdp);
            Role role = roleRepository.findById(idrole).get();
            if (role != null) {
                user.setRole(role);
                jpaUserService.save(user); //Ce service permet d'enregistrer l'utilisateur et encrypter le mdp
            }
        }
        return "redirect:/Arbitre/Categories";
    }

    //vers la page administration des utilisateurs
    @RequestMapping(value = "/Utilisateurs")
    public String administrer(Model model) {
        List<Utilisateur> utilisateurs = (List<Utilisateur>) utilisateurRepository.findAll();
        List<Role> roles = (List<Role>) roleRepository.findAll();
        model.addAttribute("listeutilisateurs", utilisateurs);
        model.addAttribute("listerole", roles);
        return "administrer";
    }

    //vers le formulaire de modification d'un utilisateur
    @RequestMapping(value = "/ModifierUtilisateur/{idutilisateur}", method = RequestMethod.GET)
    public String modificationutilisateur(Model model, @PathVariable("idutilisateur") Long idutilisateur) {
        Utilisateur user = utilisateurRepository.findById(idutilisateur).get();
        model.addAttribute("utilisateur", user);
        model.addAttribute("roles", roleRepository.findAll());
        return "modifutilisateur";
    }

    //recuperation du formulaire de modification d'un utilisateur
    @RequestMapping(value = "/ModifierUtilisateur/{idutilisateur}", method = RequestMethod.POST)
    public String modificationutilisateurmdp(Model model, @PathVariable("idutilisateur") Long idutilisateur, @RequestParam("nom") String nom, @RequestParam("mail") String email, @RequestParam Long idrole, @RequestParam(value = "reinimdp", required = false) Integer reinimdp) {
        Utilisateur user = utilisateurRepository.findById(idutilisateur).get();
        Role role = roleRepository.findById(idrole).get();

        user.setRole(role);
        user.setName(nom);
        user.setMail(email); // comment peu utiliser quelque chose pour verifier le mdp en important une dependance ? ok
        utilisateurRepository.save(user);
        Integer i = 1;
        if (i.equals(reinimdp)) {
            //TODO crée un mdp (4premieres lettre du name et les derniers caractère du mail (a partir du @ compris)
            user.setPassword(user.getName());
        }
        jpaUserService.save(user);


        return "redirect:/Admin/Utilisateurs";
    }

    //vers la gestion de toutes les versions d'une Categorie
    @RequestMapping(value = "/VoirVersions/Categorie/{id}", method = RequestMethod.GET)
    public String voirversioncat(Model model, @PathVariable("id") Long idCat) {
        Categorie derniereVersionCat = categorieRepository.findById(idCat).get();
        List<Categorie> listeVersionCat = categorieRepository.findAllByCode(derniereVersionCat.getCode());
        // On utilise categorieRepository pour acceder a la table Categorie et
        // on recupere toutes les versions d'une même Categorie ( cad toutes les cat avec le même code)
        model.addAttribute("listevs", listeVersionCat);
        //on ajoute un attribut listevs au modèle qui pour valeur la liste des != versions
        // via le modèle cet attribut serat communiquer au template
        return "toutesvscat";
    }

    //vers la gestion de toutes les versions d'une SousCategorie
    @RequestMapping(value = "/VoirVersions/Scat/{id}", method = RequestMethod.GET)
    public String voirversionscat(Model model, @PathVariable("id") Long id) {
        SousCategorie derniereVersionScat = sousCategorieRepository.findById(id).get();
        List<SousCategorie> listeVersionScat = sousCategorieRepository.findAllByNumordreAndCategorie(derniereVersionScat.getNumordre(), derniereVersionScat.getCategorie());
        // permet de crée un liste des differentes versions d'une même sous-categorie
        // a partir des données de la BDD
        model.addAttribute("listevs", listeVersionScat);
        //on donne toujour le même nom a cette attribut car on renvoi sur le meme template.
        return "toutesvscat";
    }

    @RequestMapping(value = "/VoirVersions/Regle/{id}", method = RequestMethod.GET)
    public String voirversionregle(Model model, @PathVariable("id") Long id) {
        Regle derniereVersionRegle = regleRepository.findById(id).get();
        List<Regle> listeVersionRegle = regleRepository.findAllByNumordreAndSouscategorie(derniereVersionRegle.getNumordre(), derniereVersionRegle.getSousCategorie());
        model.addAttribute("listevs", listeVersionRegle);
        return "toutesvscat";
    }

    @RequestMapping(value = "/SupprimerVersion/{id}", method = RequestMethod.GET)
    public String supprimerversioncat(Model model, @PathVariable("id") Long idCat) {
        Historique vs = historiqueRepository.findById(idCat).get();
        // je vais  chercher la version à supprimer
        if (vs.getExversion() != null) {
            // je verifie que vs n'est pas la 1er version
            Historique exvs = historiqueRepository.findById(vs.getExversion()).get();
            //je vai chercher la prochaine version

            if (vs.getNewversion() == null) {
                //si vs est la dernière version
                exvs.setNewversion(null);
                historiqueRepository.save(exvs);
            } else {
                // si vs n'est pas la dernière version
                Historique newvs = historiqueRepository.findById(vs.getNewversion()).get();
                //je vais chercher l'ancienne version
                exvs.setNewversion(newvs.getId());
                historiqueRepository.save(exvs);
                newvs.setExversion(exvs.getId());
                historiqueRepository.save(newvs);
            }
            historiqueRepository.delete(vs);
        }
        return "redirect:/Arbitre/Categories";
    }

}
