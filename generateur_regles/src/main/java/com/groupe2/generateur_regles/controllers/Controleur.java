package com.groupe2.generateur_regles.controllers;

import com.groupe2.generateur_regles.repository.*;
import com.groupe2.generateur_regles.services.JpaUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Controleur {
    //IOC ?
    @Autowired
    private JpaUserService jpaUserService;
    @Autowired
    private HistoriqueRepository historiqueRepository;
    @Autowired
    private CategorieRepository categorieRepository;
    @Autowired
    private SousCategorieRepository sousCategorieRepository;
    @Autowired
    private RegleRepository regleRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private RoleRepository roleRepository;
    @RequestMapping("/")
    public String pagecentral() {
        return "redirect:/Arbitre/Categories/";
    }

    @RequestMapping("/home")
    public String home() {
        return "redirect:/Arbitre/Categories/";
    }

    //Se connercter
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String seconnecter() {
        return "login";
    }

    // Mohamed Kori @@@@ mon commentaire, another changes ! Second version !!


    //TODO vers le formulaire de modification d'une regle (difficulté : **)
    //TODO modification d'une regle (difficulté : -sans historique *** -avec historique ****)
    // faire un premier ctrl rediririgant vers le premier form
    // un deuxieme qui recupère les infos en post et créer une nvl version de cette règle (regarder dans controleur editeur comment faire une modif)







}
