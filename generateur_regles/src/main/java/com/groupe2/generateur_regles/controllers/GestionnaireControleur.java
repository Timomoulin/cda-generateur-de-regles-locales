package com.groupe2.generateur_regles.controllers;


import com.groupe2.generateur_regles.repository.*;
import com.groupe2.generateur_regles.models.*;
import com.groupe2.generateur_regles.services.JpaUserService;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(value = "/Gestionnaire/")
public class GestionnaireControleur {
    @Autowired
    private JpaUserService jpaUserService;
    @Autowired
    private HistoriqueRepository historiqueRepository;
    @Autowired
    private CategorieRepository categorieRepository;
    @Autowired
    private SousCategorieRepository sousCategorieRepository;
    @Autowired
    private RegleRepository regleRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private RoleRepository roleRepository;

    // vers le Formulaire de modification
    @RequestMapping(value = "/Categorie/modif/{id}", method = RequestMethod.GET)
    public String modifcategorie(Model model, @PathVariable("id") Long id) {
        Categorie categorie = categorieRepository.findById(id).get();
        model.addAttribute("categorie", categorie);
        return "modifcategorie";
    }

    // modification de la categorie
    @RequestMapping(value = "/Categorie/modif/{id}", method = RequestMethod.POST)
    public String modifcategorie(Model model, @PathVariable("id") Long id, @RequestParam("title") String title) {
       /* Categorie categorie = categorieRepository.findById(id).get();
        categorie.setTitle(title);
        categorieRepository.save(categorie);*/
        Categorie exCat= categorieRepository.findById(id).get();
       Categorie nouvelleCat = new Categorie();
       nouvelleCat.setExversion(id);
       nouvelleCat.setCode(exCat.getCode());
       String titleparse= Jsoup.parse(title).text();
       nouvelleCat.setTitre(titleparse);
        //TODO Utiliser les setAutheur, setDatemodif
        categorieRepository.save(nouvelleCat);
        exCat.setNewversion(nouvelleCat.getId());
        categorieRepository.save(exCat);
        return "redirect:/Arbitre/Categories";
    }

    // vers un formulaire de création d'une categorie
    @RequestMapping(value = "/nouvelleCategorie", method = RequestMethod.GET)
    public String nouvellecategorie(Model model) {
        List<Categorie> categories = categorieRepository.findAll();
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ*";
        Integer indexcode = 0;
        ;
        String nouveaucode = "";
        for (Categorie cat : categories) {
            String code = cat.getCode();
            if (code=="Z")
            {
               return "redirect:/Arbitre/Categories";
            }
            else{
            if (alphabet.indexOf(code) > indexcode) {
                indexcode = alphabet.indexOf(code);
                nouveaucode = alphabet.substring(indexcode + 1, indexcode + 2);
            }}
        }
        model.addAttribute("code", nouveaucode);
        return "nouvellecategorie";
    }

    // ajout d'une nouvelle categorie
    @RequestMapping(value = "/nouvelleCategorie", method = RequestMethod.POST)
    public String modifcategorie(Model model, @RequestParam("code") String code, @RequestParam("title") String title) {
        Categorie cat = new Categorie();
        cat.setTitre(title);
        cat.setCode(code);
        categorieRepository.save(cat);
        return "redirect:/Arbitre/Categories";
    }
    // vers un formulaire de création d'une sous categorie
    @RequestMapping(value = "/nouvelleSousCategorie/{id}", method = RequestMethod.GET)
    public String nouvellescat(Model model, @PathVariable("id") Long id) {
        Categorie cat = categorieRepository.findById(id).get();
        Categorie catv1= categorieRepository.findAllByExversionIsNullAndCode(cat.getCode()).get(0);
        Integer numordre=0;
        for (SousCategorie scat:catv1.getSousCategories()
             ) {
            if (scat.getNumordre()>numordre)
            {
                numordre=scat.getNumordre();
            }
        }
        numordre += 1;
        model.addAttribute("numordre", numordre);
        model.addAttribute("id", id);
        return "nouvellescat";
    }

    // ajout d'une nouvelle sous cat (difficulté : ***)
    @RequestMapping(value = "/nouvelleSousCategorie", method = RequestMethod.POST)
    public String nouvellescat(Model model, @RequestParam("idcat") Long id, @RequestParam("numordre") Integer numordre, @RequestParam("title") String title, @RequestParam("object") String object) {
        Categorie cat = categorieRepository.findById(id).get();
        Categorie catv1= categorieRepository.findAllByExversionIsNullAndCode(cat.getCode()).get(0);
        SousCategorie scat = new SousCategorie();
        scat.setTitre(title);
        scat.setNumordre(numordre);
        scat.setObjet(object);
        scat.setCategorie(catv1);
        sousCategorieRepository.save(scat);
        return "redirect:/Arbitre/Categories";
    }

    //ajout d'une nouvelle regle (difficulté : ***)
    @RequestMapping(value ="/nouvelleRegle/{id}", method = RequestMethod.GET)
    public String nvlregle(Model model, @PathVariable ("id") Long id)
    {

        SousCategorie scategorie = sousCategorieRepository.findById(id).get();
        SousCategorie scatv1=sousCategorieRepository.findAllByExversionIsNullAndNumordreAndCategorie(scategorie.getNumordre(),scategorie.getCategorie()).get(0);
        Integer numerordre=0;
        for (Regle r:scatv1.getRegles()
             ) {
            if (r.getNumordre()>numerordre)
            {
                numerordre=r.getNumordre();
            }
        }
        numerordre +=1;
        model.addAttribute("numerordre", numerordre);   //créer une variable freemarker qui pourra être récupérer en {$...}
        model.addAttribute("id", id);
        return "nouvelleregle";
    }

    @RequestMapping(value = "/nouvelleRegle", method = RequestMethod.POST)
    public String nvlregle(@RequestParam("numordre") Integer numordre, @RequestParam("idscat") Long idscat, @RequestParam("corpus") String corpus)
    {
        String jsoupCorpus = Jsoup.parse(corpus).text();    // convertie le html en texte pour éviter les injections
        SousCategorie scat = sousCategorieRepository.findById(idscat).get();
        SousCategorie scatv1=sousCategorieRepository.findAllByExversionIsNullAndNumordreAndCategorie(scat.getNumordre(),scat.getCategorie()).get(0);
        Regle rgl = new Regle(scatv1, corpus, numordre);  // fait référence au constructeur du model Regle
        regleRepository.save(rgl);


        return "redirect:/Arbitre/Categories";
    }

}
