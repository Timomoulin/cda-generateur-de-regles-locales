package com.groupe2.generateur_regles.controllers;

import com.groupe2.generateur_regles.repository.RegleRepository;
import com.groupe2.generateur_regles.models.*;
import com.groupe2.generateur_regles.services.GenerateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller  //Cette annotation indique que cette classe joue le role d'un Controleur
@RequestMapping (value = "/Arbitre/")
public class ArbitreControleur {
    @Autowired
    private GenerateurService generateurService;
    //un service qui gère la création de
    // la liste de Catégorie qu'on veut afficher permet aussi de trier cette liste.
    @Autowired
    private RegleRepository regleRepository;

    /**
     * Controle vers la page de selection de règles locales
     *
     * @param model Permet de transmettre des attributs au template
     * @return le nom du template qui est utilisé pour le rendu et auquel on communique le model.
     */

    @RequestMapping("/Categories")
    public String categories(Model model) {
        List<Categorie> categories = generateurService.lastvs();
        // On utilise le service generateurService pour créer la liste de catégories.
        // Cette liste contient les dernières versions des catégories, sous-catégories et règles
        // sauf les attributs souscategories des catégories proviennent des 1ers versions.
        // De même pour les attributs règles des sous-catégories.
        // Tous les objets imbriqués contenus dans cette liste sont trié.
        model.addAttribute("categories", categories);
        return "generateur";
    }

    /** Controleur de recuperation de la selection de règle locale et envoi vers la page de génération du document
     *
     *
     * @param model Permet de transmettre des attributs au template
     * @return le nom du template qui est utilisé pour le rendu et auquel on communique le model.
     */
    @RequestMapping(value = "/Doc", method = RequestMethod.POST)
    public String genateurdoc(Model model, @RequestParam("id") List<Long> id)
    //la liste de long id contient les id des règles selectionner par l'utilisateur.
    {
        List<Regle> regles = regleRepository.findAllById(id);
        List<Categorie> categories = generateurService.listeCatDesRegles(regles);
        //On utilise le service generateurservice pour // créer la liste de catégories à partir d'une liste de règles.
        // Dans cette liste il y a seulement les catégories des règles sélectionner.
        // L'attribut souscategories de chaque catégorie ne contient que
        // des sous-catégories que si on a sélectionné au moins une règle
        // dans cette sous-catégorie.
        model.addAttribute("categories", categories);
        return "export";
    }

}
